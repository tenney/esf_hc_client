package com.fangdd.esf.hc.client.sdk;

import com.fangdd.esf.hc.idl.CallerName;
import com.fangdd.esf.hc.idl.HouseCenterResp;
import com.fangdd.esf.hc.idl.HouseCenterService.Client;
import com.fangdd.esf.hc.idl.TCallerInfo;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HouseCenterClient extends Client {
	/**
	 * 默认请求ac超时时间
	 */
	private static final int DEFAULT_TIMEOUT_MILLS = 3000;
	/**
	 * 默认请求ac域名
	 */
	private static final String DEFAULT_HOST_NAME = "127.0.0.1";
	/**
	 * 默认请求ac端口
	 */
	private static final int DEFAULT_PORT = 30025;
	/**
	 * ac注册service服务名称
	 */
	private static final String AGENT_CENTER_SERVICE_NAME = "HouseCenterService";
	/**
	 * 调用输出日志信息
	 */
	private static Logger logger = LoggerFactory
			.getLogger(HouseCenterClient.class);

	/**
	 * 放在每个client实例里边处理
	 */
	private TTransport transport;

	private HouseCenterClient(TProtocol tprotocol, TTransport ftransport)
			throws TTransportException {
		super(tprotocol);
		this.transport = ftransport;
	}
	public static HouseCenterClient getHcClient() {
		TTransport transport = null;
		logger.info("GetHouseCenterClient begin...");
		try {
			logger.info(
					"Init create a new TFramedTransport(host:{},port:{},timeout:{}ms)",
					DEFAULT_HOST_NAME, DEFAULT_PORT, DEFAULT_TIMEOUT_MILLS);

			transport = new TFramedTransport(new TSocket(DEFAULT_HOST_NAME,
					DEFAULT_PORT, DEFAULT_TIMEOUT_MILLS));

			TProtocol protocol = new TCompactProtocol(transport);

			TMultiplexedProtocol tp = new TMultiplexedProtocol(protocol,
					AGENT_CENTER_SERVICE_NAME);

			transport.open();

			return new HouseCenterClient(tp, transport);
		} catch (TTransportException e) {
			logger.error(
					"Init AgentCenterClient client failed TTransportException",
					e);
		} catch (Exception e) {
			logger.error("Init AgentCenterClient Client failed Exception", e);
		}

		return null;

	}

	/**
	 * 业务处理完毕调用close关闭方法
	 */
	public void close() {
		this.transport.close();
	}

	public static void main(String[] args) throws TException {
		HouseCenterClient client = getHcClient();
		System.out.println("设置审核状态........");
		HouseCenterResp resp = client.setHouseAuditStatus(111111L, 1, 90008L, new TCallerInfo(CallerName.BUYER, 1111L,"192.168.1.11","买家服务调用"));
		System.out.println("调用结束........服务端返回result:"+resp.toString());
		client.close();
	}

}
