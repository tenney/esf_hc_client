/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.fangdd.esf.hc.idl;

import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;
import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.server.AbstractNonblockingServer.*;

import java.util.*;

public class BatchQueryHouseBasicInfoReq implements org.apache.thrift.TBase<BatchQueryHouseBasicInfoReq, BatchQueryHouseBasicInfoReq._Fields>, java.io.Serializable, Cloneable, Comparable<BatchQueryHouseBasicInfoReq> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("BatchQueryHouseBasicInfoReq");

  private static final org.apache.thrift.protocol.TField HOUSE_IDS_FIELD_DESC = new org.apache.thrift.protocol.TField("houseIds", org.apache.thrift.protocol.TType.LIST, (short)1);
  private static final org.apache.thrift.protocol.TField TYPE_FIELD_DESC = new org.apache.thrift.protocol.TField("type", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField PAGE_INFO_FIELD_DESC = new org.apache.thrift.protocol.TField("pageInfo", org.apache.thrift.protocol.TType.STRUCT, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new BatchQueryHouseBasicInfoReqStandardSchemeFactory());
    schemes.put(TupleScheme.class, new BatchQueryHouseBasicInfoReqTupleSchemeFactory());
  }

  public List<Long> houseIds; // required
  public int type; // required
  public PageInfo pageInfo; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    HOUSE_IDS((short)1, "houseIds"),
    TYPE((short)2, "type"),
    PAGE_INFO((short)3, "pageInfo");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // HOUSE_IDS
          return HOUSE_IDS;
        case 2: // TYPE
          return TYPE;
        case 3: // PAGE_INFO
          return PAGE_INFO;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __TYPE_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.HOUSE_IDS, new org.apache.thrift.meta_data.FieldMetaData("houseIds", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64))));
    tmpMap.put(_Fields.TYPE, new org.apache.thrift.meta_data.FieldMetaData("type", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.PAGE_INFO, new org.apache.thrift.meta_data.FieldMetaData("pageInfo", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, PageInfo.class)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(BatchQueryHouseBasicInfoReq.class, metaDataMap);
  }

  public BatchQueryHouseBasicInfoReq() {
  }

  public BatchQueryHouseBasicInfoReq(
    List<Long> houseIds,
    int type,
    PageInfo pageInfo)
  {
    this();
    this.houseIds = houseIds;
    this.type = type;
    setTypeIsSet(true);
    this.pageInfo = pageInfo;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public BatchQueryHouseBasicInfoReq(BatchQueryHouseBasicInfoReq other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetHouseIds()) {
      List<Long> __this__houseIds = new ArrayList<Long>(other.houseIds);
      this.houseIds = __this__houseIds;
    }
    this.type = other.type;
    if (other.isSetPageInfo()) {
      this.pageInfo = new PageInfo(other.pageInfo);
    }
  }

  public BatchQueryHouseBasicInfoReq deepCopy() {
    return new BatchQueryHouseBasicInfoReq(this);
  }

  @Override
  public void clear() {
    this.houseIds = null;
    setTypeIsSet(false);
    this.type = 0;
    this.pageInfo = null;
  }

  public int getHouseIdsSize() {
    return (this.houseIds == null) ? 0 : this.houseIds.size();
  }

  public java.util.Iterator<Long> getHouseIdsIterator() {
    return (this.houseIds == null) ? null : this.houseIds.iterator();
  }

  public void addToHouseIds(long elem) {
    if (this.houseIds == null) {
      this.houseIds = new ArrayList<Long>();
    }
    this.houseIds.add(elem);
  }

  public List<Long> getHouseIds() {
    return this.houseIds;
  }

  public BatchQueryHouseBasicInfoReq setHouseIds(List<Long> houseIds) {
    this.houseIds = houseIds;
    return this;
  }

  public void unsetHouseIds() {
    this.houseIds = null;
  }

  /** Returns true if field houseIds is set (has been assigned a value) and false otherwise */
  public boolean isSetHouseIds() {
    return this.houseIds != null;
  }

  public void setHouseIdsIsSet(boolean value) {
    if (!value) {
      this.houseIds = null;
    }
  }

  public int getType() {
    return this.type;
  }

  public BatchQueryHouseBasicInfoReq setType(int type) {
    this.type = type;
    setTypeIsSet(true);
    return this;
  }

  public void unsetType() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __TYPE_ISSET_ID);
  }

  /** Returns true if field type is set (has been assigned a value) and false otherwise */
  public boolean isSetType() {
    return EncodingUtils.testBit(__isset_bitfield, __TYPE_ISSET_ID);
  }

  public void setTypeIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __TYPE_ISSET_ID, value);
  }

  public PageInfo getPageInfo() {
    return this.pageInfo;
  }

  public BatchQueryHouseBasicInfoReq setPageInfo(PageInfo pageInfo) {
    this.pageInfo = pageInfo;
    return this;
  }

  public void unsetPageInfo() {
    this.pageInfo = null;
  }

  /** Returns true if field pageInfo is set (has been assigned a value) and false otherwise */
  public boolean isSetPageInfo() {
    return this.pageInfo != null;
  }

  public void setPageInfoIsSet(boolean value) {
    if (!value) {
      this.pageInfo = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case HOUSE_IDS:
      if (value == null) {
        unsetHouseIds();
      } else {
        setHouseIds((List<Long>)value);
      }
      break;

    case TYPE:
      if (value == null) {
        unsetType();
      } else {
        setType((Integer)value);
      }
      break;

    case PAGE_INFO:
      if (value == null) {
        unsetPageInfo();
      } else {
        setPageInfo((PageInfo)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case HOUSE_IDS:
      return getHouseIds();

    case TYPE:
      return Integer.valueOf(getType());

    case PAGE_INFO:
      return getPageInfo();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case HOUSE_IDS:
      return isSetHouseIds();
    case TYPE:
      return isSetType();
    case PAGE_INFO:
      return isSetPageInfo();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof BatchQueryHouseBasicInfoReq)
      return this.equals((BatchQueryHouseBasicInfoReq)that);
    return false;
  }

  public boolean equals(BatchQueryHouseBasicInfoReq that) {
    if (that == null)
      return false;

    boolean this_present_houseIds = true && this.isSetHouseIds();
    boolean that_present_houseIds = true && that.isSetHouseIds();
    if (this_present_houseIds || that_present_houseIds) {
      if (!(this_present_houseIds && that_present_houseIds))
        return false;
      if (!this.houseIds.equals(that.houseIds))
        return false;
    }

    boolean this_present_type = true;
    boolean that_present_type = true;
    if (this_present_type || that_present_type) {
      if (!(this_present_type && that_present_type))
        return false;
      if (this.type != that.type)
        return false;
    }

    boolean this_present_pageInfo = true && this.isSetPageInfo();
    boolean that_present_pageInfo = true && that.isSetPageInfo();
    if (this_present_pageInfo || that_present_pageInfo) {
      if (!(this_present_pageInfo && that_present_pageInfo))
        return false;
      if (!this.pageInfo.equals(that.pageInfo))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public int compareTo(BatchQueryHouseBasicInfoReq other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetHouseIds()).compareTo(other.isSetHouseIds());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetHouseIds()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.houseIds, other.houseIds);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetType()).compareTo(other.isSetType());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetType()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.type, other.type);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetPageInfo()).compareTo(other.isSetPageInfo());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPageInfo()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.pageInfo, other.pageInfo);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("BatchQueryHouseBasicInfoReq(");
    boolean first = true;

    sb.append("houseIds:");
    if (this.houseIds == null) {
      sb.append("null");
    } else {
      sb.append(this.houseIds);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("type:");
    sb.append(this.type);
    first = false;
    if (!first) sb.append(", ");
    sb.append("pageInfo:");
    if (this.pageInfo == null) {
      sb.append("null");
    } else {
      sb.append(this.pageInfo);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws TException {
    // check for required fields
    if (houseIds == null) {
      throw new TProtocolException("Required field 'houseIds' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'type' because it's a primitive and you chose the non-beans generator.
    if (pageInfo == null) {
      throw new TProtocolException("Required field 'pageInfo' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
    if (pageInfo != null) {
      pageInfo.validate();
    }
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class BatchQueryHouseBasicInfoReqStandardSchemeFactory implements SchemeFactory {
    public BatchQueryHouseBasicInfoReqStandardScheme getScheme() {
      return new BatchQueryHouseBasicInfoReqStandardScheme();
    }
  }

  private static class BatchQueryHouseBasicInfoReqStandardScheme extends StandardScheme<BatchQueryHouseBasicInfoReq> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, BatchQueryHouseBasicInfoReq struct) throws TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // HOUSE_IDS
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list0 = iprot.readListBegin();
                struct.houseIds = new ArrayList<Long>(_list0.size);
                for (int _i1 = 0; _i1 < _list0.size; ++_i1)
                {
                  long _elem2;
                  _elem2 = iprot.readI64();
                  struct.houseIds.add(_elem2);
                }
                iprot.readListEnd();
              }
              struct.setHouseIdsIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // TYPE
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.type = iprot.readI32();
              struct.setTypeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // PAGE_INFO
            if (schemeField.type == org.apache.thrift.protocol.TType.STRUCT) {
              struct.pageInfo = new PageInfo();
              struct.pageInfo.read(iprot);
              struct.setPageInfoIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetType()) {
        throw new TProtocolException("Required field 'type' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, BatchQueryHouseBasicInfoReq struct) throws TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.houseIds != null) {
        oprot.writeFieldBegin(HOUSE_IDS_FIELD_DESC);
        {
          oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.I64, struct.houseIds.size()));
          for (long _iter3 : struct.houseIds)
          {
            oprot.writeI64(_iter3);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(TYPE_FIELD_DESC);
      oprot.writeI32(struct.type);
      oprot.writeFieldEnd();
      if (struct.pageInfo != null) {
        oprot.writeFieldBegin(PAGE_INFO_FIELD_DESC);
        struct.pageInfo.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class BatchQueryHouseBasicInfoReqTupleSchemeFactory implements SchemeFactory {
    public BatchQueryHouseBasicInfoReqTupleScheme getScheme() {
      return new BatchQueryHouseBasicInfoReqTupleScheme();
    }
  }

  private static class BatchQueryHouseBasicInfoReqTupleScheme extends TupleScheme<BatchQueryHouseBasicInfoReq> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, BatchQueryHouseBasicInfoReq struct) throws TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      {
        oprot.writeI32(struct.houseIds.size());
        for (long _iter4 : struct.houseIds)
        {
          oprot.writeI64(_iter4);
        }
      }
      oprot.writeI32(struct.type);
      struct.pageInfo.write(oprot);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, BatchQueryHouseBasicInfoReq struct) throws TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      {
        org.apache.thrift.protocol.TList _list5 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.I64, iprot.readI32());
        struct.houseIds = new ArrayList<Long>(_list5.size);
        for (int _i6 = 0; _i6 < _list5.size; ++_i6)
        {
          long _elem7;
          _elem7 = iprot.readI64();
          struct.houseIds.add(_elem7);
        }
      }
      struct.setHouseIdsIsSet(true);
      struct.type = iprot.readI32();
      struct.setTypeIsSet(true);
      struct.pageInfo = new PageInfo();
      struct.pageInfo.read(iprot);
      struct.setPageInfoIsSet(true);
    }
  }

}

