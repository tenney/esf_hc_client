/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.fangdd.esf.hc.idl;


import org.apache.thrift.TEnum;

public enum CallerName implements TEnum {
  BUYER(1),
  SELLER(2),
  AGENT(3),
  ASSOCIATOR(4);

  private final int value;

  private CallerName(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of this enum value, as defined in the Thrift IDL.
   */
  public int getValue() {
    return value;
  }

  /**
   * Find a the enum type by its integer value, as defined in the Thrift IDL.
   * @return null if the value is not found.
   */
  public static CallerName findByValue(int value) {
    switch (value) {
      case 1:
        return BUYER;
      case 2:
        return SELLER;
      case 3:
        return AGENT;
      case 4:
        return ASSOCIATOR;
      default:
        return null;
    }
  }
}
